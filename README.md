These scripts comprise part of a pipeline to plot sequence conservation between aligned sequences (i.e. plot the number of variant sites across
a user defined sliding window). In the Bachmann et al. (2018) paper, the alignments were created using LASTZ, resulting in alignments with the 
*.aln format. Given the functionality of Biopython however, a number of other alignment ormats are supported.

To create a plot of sequence conservation, the following steps are required:

1) run the S-locus_onservation_Bachmann_G3.py script to calculate the number of vairant sites between your alignments (multiple pairwise alignments
are accepted as long as the first sequence in each pair is constant).

e.g. python /path/to/S-locus_conservation_Bachmann_G3.py -f filename 

2) use the resultant file to create the plot. Please note the R script requires modification at the places indicating.