#!/usr/bin/env python
import sys, os, re, itertools, getopt
from Bio import SeqIO

def compare(string1, string2, no_match_c='*', match_c='|'):
    result = ''
    n_diff = 0
    for c1, c2 in itertools.izip(string1, string2):
        if c1 == c2:
            result += match_c
        else:
            result += no_match_c
            n_diff += 1
    delta = len(string2) - len(string1)
    result += delta * no_match_c
    n_diff += delta
    return (result, n_diff)
    
def chunks(seq, win, step):
    seqlen = len(seq)
    for i in range(0,seqlen,step):
        j = seqlen if i+win>seqlen else i+win
        yield seq[i:j]
        if j==seqlen: break


def main():
    file = 0
    outfile = 0
    window_size = 0
    verbose = False
    type = 0
    
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hvf:o:t:w:', ['help=', 'verbose=', 'file=', 'outfile=', 'type=', 'window_size='])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit(2)
        elif opt in ('-f', '--file'):
            file = arg
        elif opt in ('-o', '--outfile'):
            outfile = arg
        elif opt in ('-w', '--window_size'):
            window_size = arg
        elif opt in ('-t', '--type'):
            type = arg
        elif opt in ('-v', '--verbose'):
            verbose = True
            
    if file == 0:
        sys.stderr.write("No input alignment given! %s \n" % file)
        sys.exit(2)
        
    if window_size == 0:
        window_size = 250
        sys.stderr.write("Using default window size %s \n" % window_size)
        
    if outfile == 0:
        outfile = ("{0}_{1}_window_conservation.txt".format(os.path.splitext(file)[0], str(window_size)))
        sys.stderr.write("Using default output name %s \n" % outfile)
        
    if type == 0:
        type = "clustal"
        sys.stderr.write("Using default alignment type %s \n" % type)

    output = open(outfile, "w")
        
    records = list(SeqIO.parse(file, type))
    for x in range(1, len(records), 1):
        if verbose:
            print("{0} vs. {1}".format(records[0].id, records[x].id))
        if int(x) == 1:
            output.write("{0} vs. {1},".format(records[0].id, records[x].id))
        else:
            output.write("\n{0} vs. {1},".format(records[0].id, records[x].id))
        seq0 = records[0].seq
        subseq =list(chunks(seq0, int(window_size), int(window_size)))
        seq1 = records[x].seq
        subseq1 =list(chunks(seq1, int(window_size), int(window_size)))
        for y in range(0, len(subseq), 1):
            string1 = str(subseq[y])
            string2 = str(subseq1[y])
    
            result, n_diff = compare(string1, string2, no_match_c='*')
            if verbose:
                print("{0}\n{1}\n{2}\n{3}".format(str(subseq[y]), result, str(subseq1[y]), str(n_diff).strip()))
            output.write("{0},".format(str(n_diff)))
    output.write("\n")

def usage(): 
    print( """ 	
______________________________________________________________________________
                      S-locus_conservation_Bachmann_G3.py
                               by Andy Tedder
______________________________________________________________________________
When provided with a multi-sequence LASTZ alignment file (typically *.aln; 
'-f') this script will calculate sequence conservation across user a specified
sliding-window('-w') length. Other alignment formats may be supported using 
the optional '-t' flag, see SeqIO manual for a list of formats.
==============================================================================

-h  : help
-f  : file
-o  : outfile (Default available)
-w  : window_size (Default: 250 bp)
-s  : step_size (Default: 50 bp)
-t  : type - sequence input format (Default: clustal)
-v  : verbose
______________________________________________________________________________
    			
             	 
                           """)
            
    

        
if __name__ == "__main__":
    main()